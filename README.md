# Getting started

This repository contains boilerplate for creating custom carrier integrations with Jetti.io. To get started, you'll first need to register your custom service in the Jetti integrations section:

https://app.jetti.io/shipping-integrations

To get started with the examples, you'll need to `npm install` or `yarn`. To run the dev server, run `PORT=8000 npm run start:dev` or `PORT=8000 yarn start:dev`.

The port number can be adjusted if needed. For testing, you may want to use something like https://ngrok.com/ to call the local setup from your Jetti API account.

## Generating rates
The rates endpoint (e.g. `POST /rates.json` in `src/index.js`) returns a list of available rates for a shipment. The endpoint will be sent a payload of data, containing the address and shipment details:

```json
{
	"company": {
		"name": "jetti-imac-1",
		"email": "support@jetti.io"
	},
	"parcelWeight": "0.1000",
	"toAddress": {
		"email": "vendor@email.com",
		"phone": "",
		"name": "Jack Jones",
		"firstName": "Jack",
		"lastName": "Jones",
		"addressLineOne": "1600 Pennsylvania Ave",
		"addressLineTwo": null,
		"city": "NW Washington",
		"state": "CA",
		"country": "US",
		"zip": "20500",
		"company": "Jack Inc"
	},
	"fromAddress": {
		"email": "vendor@email.com",
		"phone": "",
		"returnInHouse": true,
		"addressLineOne": "1051 S Coast Hwy 101 B",
		"addressLineTwo": "",
		"city": "Encinitas",
		"state": "CA",
		"country": "US",
		"zip": "92024",
		"company": "Jetti dropship"
	},
	"dimensions": {
		"length": 12.4,
		"width": 10.9,
		"height": 1.5,
		"distanceUnit": "in",
		"massUnit": "lb",
		"weight": "0.10"
	},
	"orderValue": 9.99,
	"iso": "USD"
}
```

This data can then be used to query external carrier integrations to return back to Jetti a list of available rates. A response must be received in 20 seconds. Multiple rates can be returned if needed, by passing in additional items in the `rates` array.

```json
{
	"rates": [{
		"price": 10.55,
		"provider": "Jetti Provider",
		"providerId": "jetti_provider",
		"quoteId": "abc123",
		"test": false,
		"serviceLevel": "First Class",
		"serviceLevelToken": "first_class"
	}]
}
```

## Printing a shipping label

Once a rate has been selected by the user, the `POST label.json` endpoint is then called with the following payload. The rateId maps back to the `quoteId` included in the previous `rates.json` response. You'll also find included the same data sent in the original `rates.json` request (such as the shipment weight) in case this is needed in the generation of the label.

```json
{
	"rateId": "abc123",
	"rate": {
		"test": false,
		"price": 10.55,
		"quoteId": "abc123",
		"provider": "Jetti Provider",
		"providerId": "jetti_provider",
		"serviceLevel": "First Class",
		"serviceLevelToken": "first_class",
		"shippingIntegrationId": 1
	},
	"toAddress": {
		"zip": "20500",
		"city": "NW Washington",
		"name": "Jon Bolt",
		"email": "vendor@email.com",
		"phone": "",
		"state": "CA",
		"company": "Jack Inc",
		"country": "US",
		"lastName": "Bolt",
		"firstName": "Jon",
		"addressLineOne": "1600 Pennsylvania Ave",
		"addressLineTwo": null
	},
	"fromAddress": {
		"zip": "92024",
		"city": "Encinitas",
		"email": "vendor@email.com",
		"phone": "",
		"state": "CA",
		"company": "Jetti dropship",
		"country": "US",
		"returnInHouse": true,
		"addressLineOne": "1051 S Coast Hwy 101 B",
		"addressLineTwo": ""
	},
	"dimensions": {
		"width": 10.9,
		"height": 1.5,
		"length": 12.4,
		"weight": "0.10",
		"massUnit": "lb",
		"distanceUnit": "in"
	},
	"parcelWeight": "0.10",
	"orderValue": "9.99",
	"iso": "USD"
}
```

This data can then be used to call an external service to generate a tracking number and shipping label. Jetti expects the data to be returned within 20 seconds.

```json
{
	"label": {
		"externalId": "abc123",
		"trackingCompany": "Royal Mail",
		"trackingNumber": "123ABC",
		"serviceLevel": "First Class",
		"serviceLevelToken": "first_class",
		"price": 10.99,
		"labelUrl": "http://shipping.com/label.png",
		"labelFiles": ["http://shipping.com/returns.png"]
	}
}
```
