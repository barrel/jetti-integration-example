import bodyParser from 'body-parser';
import express from 'express';
import pino from 'pino';

const logger = pino({
    prettyPrint: true,
});

const app = express();
const port = parseInt(process.env.PORT, 10) || 8000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));

// Returns a list of available rates
app.post('/rates.json', (req, res) => {
    logger.info('Received rates request', req.body);
    const rates = [{
        price: 10.55,
        provider: 'Jetti Provider',
        providerId: 'jetti_provider',
        quoteId: 'abc123',
        test: false,
        serviceLevel: 'First Class',
        serviceLevelToken: 'first_class',
    }];
    logger.info('Returning rates', { rates });
    return res.json({ rates });
});

// Returns a list of available rates
app.post('/label.json', (req, res) => {
    logger.info('Received label request', req.body);
    const label = {
        externalId: 'abc123',
        trackingCompany: 'Royal Mail',
        trackingNumber: '123ABC',
        serviceLevel: 'First Class',
        serviceLevelToken: 'first_class',
        price: 10.99,
        labelUrl: 'http://shipping.com/label.png',
        labelFiles: ['http://shipping.com/returns.png'],
    };
    logger.info('Returning label', { label });
    return res.json({ label });
});

app.listen(port, () => logger.info(`App listening on port ${port}`));
